
/******************
nombre: Main screen
descripcion:
  Muestra el intro de Hello World Technologies
  Depués muestra el menu, con los botones.
*******************/
class MainScreen {

  Levels lvl;

  boolean intro;

  float flashX = 30;

  float introY = -100;

  boolean impactHappened = false;

  boolean sliceHappened = false;

  int introTimer = 0;

  boolean menuScreen;
  boolean levelScreen;



  boolean muted = false;

  
  FancyButton g;


  MainScreen() {

   lvl = new Levels();
    g = new FancyButton();
    setScreen("intro");
  }

  void display() {
    if (intro) {
      displayIntro();
    }
    //setBallSkin();
    if (menuScreen) {
      displayMenu();
    }
    if (levelScreen) {
      lvl.display();
      if (lvl.showLevels) {
        backButton();
      }
    }
  }

  void displayIntro() {
    noStroke();
    introTimer++;
    fill(255);
    rect(0, 0, width, height);
    if (introY < height/2) {
      introY += 10;
      introTimer = 0;
    }
    if (introY >= height/2) {
      if (impactHappened == false) {
        impactHappened = true;
        if (muted == false) {
        }
      }
      if (introTimer > 50) {
        flashX += 30;
        if (sliceHappened == false) {
          sliceHappened = true;
          if (muted == false) {
          }
        }
      }
    }
    fill(0);
    textSize(20);
    text("a game by", width/2 - textWidth("a game by"), introY - 27);
    textSize(48);
    fill(50, 160, 230);
    text("Hello World Technologies", width/2, introY);
    textSize(14);
    fill(255, 100);
    beginShape();
    vertex(flashX, height/2+13);
    vertex(flashX + 20, height/2+13);
    vertex(flashX + 50, height/2+13 - 48);
    vertex(flashX + 30, height/2+13 - 48);
    endShape();
    beginShape();
    vertex(flashX-20, height/2+13);
    vertex(flashX - 10, height/2+13);
    vertex(flashX + 20, height/2+13 - 48);
    vertex(flashX + 10, height/2+13 - 48);
    endShape();

    if (introTimer > 130) {
      float trans = map(introTimer, 130, 230, 0, 255);
      fill(255, trans);
      rect(0, 0, width, height);
    }

    if (introTimer > 240) {
      setScreen("menuScreen");
    }
  }

  void displayMenu() {
    fill(0);
    textSize(48);
    text("Football Game", width/2, 84);
    textSize(30);
    createButton(0, width/2-125, height/2-140, 250, 100, 7, "Play", color(0), color(212, 175, 55));
    textSize(14);
    fill(0);
    text("HWT", 50, height-20);
  }

  void backButton() {
    createButton(3, 40, 40, 60, 30, 6, "Back", color(0), color(212, 175, 55));
  }

  void setScreen(String s) {
    if (s.equals("intro")) {
      intro = true; 
      menuScreen = false;
      levelScreen = false;
    
    }
    if (s.equals("menuScreen")) {
      intro = false; 
      menuScreen = true;
      levelScreen = false;
     
    }
    if (s.equals("levelScreen")) {
      intro = false; 
      menuScreen = false;
      levelScreen = true;
     
    }
  
  }

  void createButton(int i, int x, int y, int w, int h, int r, String s, color normal, color highlighted) {
    noStroke();
    g.iRect(i, x, y, w, h, r, normal, highlighted, "Hellou");
    
  }
  void mPressed() {
    if (menuScreen && mouseX >= width/2-125 && mouseX <= width/2+125 && mouseY >= height/2-140 && mouseY <= height/2-40) {
      setScreen("levelScreen");
    }

  }
  
}