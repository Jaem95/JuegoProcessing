import processing.sound.*;
SoundFile file;
//PruebaRepo en dev

// player motion
float x = 10000, dx = -20, ddx = 0;
float y = 10000, dy = -20, ddy = 0;
float z = 20;
//trees
int x1=200, f=2, prig=0, c=850, sch=2252;
int xd=-100, yd1=500, yd=500;  //trees
// (0, 0) point of the current "quadrant"
int qx, qy;
// player angles
float h = PI + QUARTER_PI, v = 0;
final float angleUnit = TAU / 128;
// ball motion
float bx = 9850, bdx = 0;
float by = 9850, bdy = 0;
// stuck prevention between the player and the ball
boolean stuck = false;
// constant for daylight cycle
final float frameCountToSine = TAU / 1000;
// optimization
boolean wPressed = false, sPressed = false, aPressed = false, dPressed = false, leftPressed = false, rightPressed = false, upPressed = false, downPressed = false;
PImage sky;

PFont font;
MainScreen ms;

void setup() {
  //size(600, 600);
  textAlign(CENTER, CENTER);
   size(900, 600, P3D);
 // fullScreen(P3D);
  perspective(PI/3, float(width)/height, 1, 900);
    sky=loadImage("Skybox2.png");
    file = new SoundFile(this, "Estadio.mp3");
    file.play();

  ms = new MainScreen();
}

void draw() {
  background(255);
  ms.display();
  
}

/*void keyPressed() {
  ms.kPressed();
}
void keyReleased() {
  ms.kReleased();
}
*/
void mousePressed() {
  ms.mPressed();
}


void keyPressed() {
  if      (key == 'w')       wPressed = true;
  else if (key == 's')       sPressed = true;
  else if (key == 'a')       aPressed = true;
  else if (key == 'd')       dPressed = true;
  else if (keyCode == LEFT)  leftPressed = true;
  else if (keyCode == RIGHT) rightPressed = true;
  else if (keyCode == DOWN)  downPressed = true;
  else if (keyCode == UP)    upPressed = true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void keyReleased() {
  if      (key == 'w')       wPressed = false;
  else if (key == 's')       sPressed = false;
  else if (key == 'a')       aPressed = false;
  else if (key == 'd')       dPressed = false;
  else if (keyCode == LEFT)  leftPressed = false;
  else if (keyCode == RIGHT) rightPressed = false;
  else if (keyCode == DOWN)  downPressed = false;
  else if (keyCode == UP)    upPressed = false;
}




/***************************************************************************************************************************/


/**************************************************************************************************************************/