
class Levels {



  FancyButton g;
  boolean showLevels = true;



  Levels() {


    g = new FancyButton();
  }
  void displayFootballGame()
  {
     background(200);
  
 //drawSkybox(1000,1000,1000);
  
  
  
  // player motion
  if (wPressed) {
    ddx = cos(h);
    ddy = sin(h);
  }
  else if (sPressed) {
    ddx = -cos(h);
    ddy = -sin(h);
  }
  else if (aPressed) {
    ddx = cos(h-HALF_PI);
    ddy = sin(h-HALF_PI);
  }
  else if (dPressed) {
    ddx = cos(h+HALF_PI);
    ddy = sin(h+HALF_PI);
  }
  else {
    ddx = 0;
    ddy = 0;
  }
  if      (leftPressed)  h -= angleUnit;
  else if (rightPressed) h += angleUnit;
  if      (downPressed)  v -= angleUnit;
  else if (upPressed)    v += angleUnit;
  v = constrain(v, -0.4, 0.4);
  
  
    
  
  dx = dx*0.9 + ddx;
  dy = dy*0.9 + ddy;
  x += dx;
  y += dy;
  
  // ball motion
  bdx *= 0.99;
  bdy *= 0.99;
  bx += bdx;
  by += bdy;
  
  // collision detection
  if (sqrt(sq(x - bx) + sq(y - by)) < 40) {
    if (!stuck) {
      stuck = true;
      float tx = dx;
      float ty = dy;
      dx = bdx;
      dy = bdy;
      bdx = tx;
      bdy = ty;
      println("Me estas tocando");
    }
  }
  else    stuck = false;
  
  camera(x, y, z,    x + cos(h)*cos(v), y + sin(h)*cos(v), z + sin(v),    0, 0, -1);
  // a zone of nine 900x900 quadrants, the player being in the middle one. the 8 around him are there to avoid getting close to the edge
  qx = int(x/900) * 900 - 900;
  qy = int(y/900) * 900 - 900;
  translate(qx, qy);
  
  //lights();
  //float daylight = (sin(frameCount * frameCountToSine) * 64) + 128;

  fill((x+y)/100); // it gets darker near (0, 0)
  rect(0, 0, 2700, 2700);
  
  // only draw what you can see
  stroke(32);
  for (int l = 0; l < 2700; l+=20) {
    line(0, l, 2700, l);
    line(l, 0, l, 2700);
  }
  
  translate(-qx+bx, -qy+by, 20); // minus the previous translate
  fill(255, 255, 0);
  noStroke();
  sphere(20);
  }

  void display() {
    if (showLevels) {
      displayFootballGame();
    }
   
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



}